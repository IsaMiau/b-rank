package cl.brank.brank;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Debug;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.login.LoginManager;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import org.threeten.bp.LocalDate;
import org.threeten.bp.Period;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import cl.brank.brank.models.Categoria;
import cl.brank.brank.models.UserValues;
import cl.brank.brank.preferences.SesionPreferencesUser;
import cl.brank.brank.util.Util;

public class CompletarPerfilActivity extends AppCompatActivity {


    private FirebaseAuth mAuth;

    private DatabaseReference mDatabase;

    private TextView txtNombre, txtCategoria;
    private Button btnSiguiente, btnCerrarSesion, btnFechaNacimiento;

    private Spinner spnLocalidades;

    private List<Categoria> categorias;
    private List<Categoria> localidades;

    private ImageView imgPerfil;

    private UserValues userValues;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_completar_perfil);

        mAuth = FirebaseAuth.getInstance();

        userValues = new UserValues();

        txtNombre = (TextView)findViewById(R.id.txtNombre);
        txtNombre.setText(mAuth.getCurrentUser().getDisplayName());
        userValues.setName(txtNombre.getText().toString());

        btnSiguiente = (Button)findViewById(R.id.btnSiguiente);
        btnSiguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                irAAjustesDeBusquedas();
            }
        });

        btnCerrarSesion = (Button)findViewById(R.id.btnCerrarSesion);
        btnCerrarSesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAuth.signOut();
                LoginManager.getInstance().logOut();
                cerrarSesion();
            }
        });

        btnFechaNacimiento = (Button)findViewById(R.id.btnFechaNacimiento);
        btnFechaNacimiento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().show();
            }
        });

        txtCategoria = (TextView) findViewById(R.id.txtCategoria);
        spnLocalidades= (Spinner)findViewById(R.id.spnLocalidades);

        readCategoriasYLocalidades();

        List<Categoria> aux = new ArrayList<>();
        aux.add(new Categoria(0, "Obteniendo datos"));
        //loadSpinner(spnCategoria, aux);
        loadSpinner(spnLocalidades, aux);

        imgPerfil = (ImageView)findViewById(R.id.imgPerfil);
        Picasso.with(this).load(mAuth.getCurrentUser().getPhotoUrl()).placeholder(R.mipmap.ic_launcher).into(imgPerfil);
        userValues.setUrlAvatar(mAuth.getCurrentUser().getPhotoUrl().toString());




    }

    private void irAAjustesDeBusquedas() {
        if(btnFechaNacimiento.getText().toString().length()>0) {
            userValues.setCategory(txtCategoria.getText().toString());
            userValues.setLocation(spnLocalidades.getSelectedItem().toString());
            SesionPreferencesUser.getInstance().saveUserSesion(this, userValues);
            Intent i = new Intent(this, AjustesDeBusquedaActivity.class);
            startActivity(i);
        }else{
            Toast.makeText(this, "Ingrese fecha de nacimiento", Toast.LENGTH_SHORT).show();
        }
    }

    private void readCategoriasYLocalidades() {
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.child("categorias").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                    if(categorias == null){
                        categorias = new ArrayList<Categoria>();
                    }
                    Categoria categoria = postSnapshot.getValue(Categoria.class);
                    categorias.add(categoria);
                }

                if(categorias == null){
                    categorias = new ArrayList<>();
                    categorias.add(new Categoria(0, "No hay Categoria"));
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mDatabase.child("localidades").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                    if(localidades == null){
                        localidades = new ArrayList<Categoria>();
                    }
                    Categoria categoria = postSnapshot.getValue(Categoria.class);
                    localidades.add(categoria);
                }

                if(localidades == null){
                    localidades = new ArrayList<>();
                    localidades.add(new Categoria(0, "No hay Localidades"));
                }


                loadSpinner(spnLocalidades, localidades);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void loadSpinner(Spinner spn, List<Categoria> values) {
        ArrayAdapter<Categoria> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, values);
        spn.setAdapter(adapter);
    }

    private void cerrarSesion() {
        Intent i = new Intent(this, LoginActivity.class);
        startActivity(i);
        finish();
    }


    private DatePickerDialog getDialog(){
        Calendar c = Calendar.getInstance();
        int anio = c.get(Calendar.YEAR)-18;
        int mes = c.get(Calendar.MONTH);
        int dia = c.get(Calendar.DAY_OF_MONTH);
        if(userValues.getDate()!=null){
            anio = userValues.getDate().getYear();
            mes = userValues.getDate().getMonthValue()-1;
            dia = userValues.getDate().getDayOfMonth();
        }

        return new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

                btnFechaNacimiento.setText(year + "/" + formatDate((month + 1)) + "/"+ formatDate(dayOfMonth));
                userValues.setDate(LocalDate.of(year, month+1, dayOfMonth));
                cargaCategoria(Util.getInstance().getEdad(userValues.getDate()));


            }
        }, anio,mes,dia);

    }

    private void cargaCategoria(int edad) {
        String categoriaAux = "";
        for(Categoria aux : categorias){
            if(edad < aux.getEdad()){
                categoriaAux = aux.getDescripcion();
                break;
            }
        }
        txtCategoria.setText(categoriaAux);
    }

    private String formatDate(int value){
        return (value>9?value+"":"0"+value);
    }

}
