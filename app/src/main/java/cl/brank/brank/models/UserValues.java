package cl.brank.brank.models;

import com.google.gson.Gson;

import org.threeten.bp.LocalDate;

/**
 * Created by hardroidlabs on 01-03-17.
 */
public class UserValues {
    private String name;
    private LocalDate date;
    private String email;
    private String urlAvatar;
    private String category;
    private String location;

    public UserValues(String name, LocalDate date, String email, String urlAvatar, String category, String location) {
        this.name = name;
        this.date = date;
        this.email = email;
        this.urlAvatar = urlAvatar;
        this.category = category;
        this.location = location;
    }

    public UserValues() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getUrlAvatar() {
        return urlAvatar;
    }

    public void setUrlAvatar(String urlAvatar) {
        this.urlAvatar = urlAvatar;
    }

    @Override
    public String toString() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }

    public static UserValues getUserValues(String json) {
        Gson gson = new Gson();
        try {
            UserValues values = gson.fromJson(json, UserValues.class);
            return values;
        }catch (Exception e) {
            return null;
        }
    }
}
