package cl.brank.brank;

import android.app.Application;
import android.support.multidex.MultiDex;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.jakewharton.threetenabp.AndroidThreeTen;

/**
 * Created by harttynarce on 11-01-17.
 */

public class BRankApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        //FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        AndroidThreeTen.init(this);
        MultiDex.install(this);
    }
}